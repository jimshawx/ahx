#include <windows.h>

#include "AHX/Windows.h"
#include "AHX/AHX.h"

BOOL WINAPI DllMain(HINSTANCE hinstDLL, DWORD fdwReason, LPVOID lpReserved)
{
	//switch (fdwReason)
	//{
	//	case DLL_PROCESS_ATTACH: break;
	//	case DLL_THREAD_ATTACH: break;
	//	case DLL_THREAD_DETACH: break;
	//	case DLL_PROCESS_DETACH: break;
	//}
	return TRUE;
}

int main(void)
{
	auto player = new AHXPlayer();
	player->Init();
	player->LoadSong((char *)("m0d_-_omc_meat_pie.ahx"));
	player->InitSubsong(0);

	for (int i = 0; i < 30; i++)
		player->NextPosition();
	
	auto output = new AHXWaveOut();
	output->Init();
	output->Play(player);

	output->StartBackgroundPlay();

	for (;;)
		Sleep(1000);
	
	return 0;
}

extern "C"
{
	__declspec(dllexport) AHXPlayer *__stdcall CreateAHXPlayer()
	{
		return new AHXPlayer;
	}
	__declspec(dllexport) void __stdcall DisposeAHXPlayer(AHXPlayer *player)
	{
		delete player;
	}

	__declspec(dllexport) void __stdcall InitAHXPlayer(AHXPlayer *player, AHXWaves *Waves) { player->Init(Waves); }

	__declspec(dllexport) int __stdcall LoadSongFromFile(AHXPlayer *player, char *Filename) { return player->LoadSong(Filename); }
	__declspec(dllexport) int __stdcall LoadSongFromMemory(AHXPlayer *player, void *Buffer, int Len) { return player->LoadSong(Buffer, Len); }
	__declspec(dllexport) int __stdcall InitSubsong(AHXPlayer *player, int Nr) { return player->InitSubsong(Nr); }
	__declspec(dllexport) void __stdcall PlayIRQ(AHXPlayer *player) { player->PlayIRQ(); }

	__declspec(dllexport) void __stdcall VoiceOnOff(AHXPlayer *player, int Voice, int OnOff) { player->VoiceOnOff(Voice, OnOff); }
	__declspec(dllexport) void __stdcall NextPosition(AHXPlayer *player) { player->NextPosition(); }
	__declspec(dllexport) void __stdcall PrevPosition(AHXPlayer *player) { player->PrevPosition(); }

	__declspec(dllexport) int __stdcall getPlayingTime(AHXPlayer *player) { return player->PlayingTime; }
	__declspec(dllexport) void __stdcall setPlayingTime(AHXPlayer *player, int PlayingTime) { player->PlayingTime = PlayingTime; }
	__declspec(dllexport) AHXSong *__stdcall getSong(AHXPlayer *player) { return &player->Song; }
	__declspec(dllexport) void __stdcall setSong(AHXPlayer *player, AHXSong *Song) { player->Song = *Song; }

	__declspec(dllexport) AHXWaveOut *__stdcall CreateAHXWaveOut()
	{
		return new AHXWaveOut;
	}
	__declspec(dllexport) void __stdcall DisposeAHXWaveOut(AHXWaveOut *waveOut)
	{
		delete waveOut;
	}

	/*
	__declspec(dllexport) int InitAHXWaveOut(AHXWaveOut *waveOut) { return waveOut->Init(); }
	__declspec(dllexport) int InitAHXWaveOut(AHXWaveOut *waveOut, int Frequency) { return waveOut->Init(Frequency); }
	__declspec(dllexport) int InitAHXWaveOut(AHXWaveOut *waveOut, int Frequency, int Bits) { return waveOut->Init(Frequency, Bits); }
	__declspec(dllexport) int InitAHXWaveOut(AHXWaveOut *waveOut, int Frequency, int Bits, int Frames) { return waveOut->Init(Frequency, Bits, Frames); }
	__declspec(dllexport) int InitAHXWaveOut(AHXWaveOut *waveOut, int Frequency, int Bits, int Frames, int NrBlocks) { return waveOut->Init(Frequency, Bits, Frames, NrBlocks); }
	__declspec(dllexport) int InitAHXWaveOut(AHXWaveOut *waveOut, int Frequency, int Bits, int Frames, int NrBlocks, float Boost) { return waveOut->Init(Frequency, Bits, Frames, NrBlocks, Boost); }
	*/
	__declspec(dllexport) int InitAHXWaveOut(AHXWaveOut *waveOut, int Frequency, int Bits, int Frames, int NrBlocks, float Boost, int Hz) { return waveOut->Init(Frequency, Bits, Frames, NrBlocks, Boost, Hz); }
	__declspec(dllexport) int Free(AHXWaveOut *waveOut) { return waveOut->Free(); }
	__declspec(dllexport) int StartBackgroundPlay(AHXWaveOut *waveOut) { return waveOut->StartBackgroundPlay(); }
	__declspec(dllexport) int StopBackgroundPlay(AHXWaveOut *waveOut) { return waveOut->StopBackgroundPlay(); }
	__declspec(dllexport) int Play(AHXWaveOut *waveOut, AHXPlayer *Player) { return waveOut->Play(Player); }
	__declspec(dllexport) int Pause(AHXWaveOut *waveOut) { return waveOut->Pause(); }
	__declspec(dllexport) int Resume(AHXWaveOut *waveOut) { return waveOut->Resume(); }
	__declspec(dllexport) int Stop(AHXWaveOut *waveOut) { return waveOut->Stop(); }
	__declspec(dllexport) int SetVolume(AHXWaveOut *waveOut, int Volume) { return waveOut->SetVolume(Volume); }
	__declspec(dllexport) int CopyBuffer(AHXWaveOut *waveOut, void *lpBuffer, int *lpValid) { return waveOut->CopyBuffer(lpBuffer, lpValid); }

}